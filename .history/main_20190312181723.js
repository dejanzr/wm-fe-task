$(document).ready(function () {
  // Setup - add a text input to each footer cell
  $('#root tfoot th').each(function () {
    var title = $(this).text();
    $(this).html('<input type="text" placeholder="Search ' + title + '" />');
  });

  // DataTable
  var table = $('#root').DataTable();
  console.log(table)
  // Apply the search
  table.columns().every(function () {
    var that = this;
    $('input', this.header()).on('keyup change', function () {
      if (that.search() !== this.value) {
        that
          .search(this.value)
          .draw();
      }
    });
  });
});
