// $(document).ready(function () {
//   // Setup - add a text input to each footer cell
//   $('#root thead th').each(function () {
//     var title = $(this).text();
//     $(this).html('<input type="text" placeholder="Search ' + title + '" />');
//   });
//
//   // DataTable
//   var table = $('#root').DataTable({
//     // Configure datatable.
//   dom: 'frtlip' // Show entries.
//   });
//   // Apply the search
//   table.columns().every(function () {
//     var that = this;
//     $('input', this.footer()).on('keyup change', function () {
//       if (that.search() !== this.value) {
//         that
//           .search(this.value)
//           .draw();
//       }
//     });
//   });
// });

$(document).ready( function () {
  // Setup - add a text input to each header cell
  $('#root thead tr:eq(1) th').each( function () {
    var title = $('#root thead tr:eq(0) th').eq( $(this).index() ).text();
    $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
  } );


  var table = $('#root').DataTable({
    orderCellsTop: false,
    "bInfo" : false, // Showing 1 of n entries - hide.
    "bSortCellsTop": false,
    dom: 'frtlip', // Show entries.
    language: {
      paginate: {
        next: '<img class="data-pagination-ico" src="icons/next.svg">',
        previous: '<img class="data-pagination-ico" src="icons/prev.svg">'
      }
    },
  });
  // Apply the search
  table.columns().every(function (index) {
    $('#root thead tr:eq(1) th:eq(' + index + ') input').on('keyup change', function () {
      table.column($(this).parent().index() + ':visible')
        .search(this.value)
        .draw();
    });
  });

  var btn = document.getElementById('mile');
  btn.addEventListener('click', function (params) {
    table.row.add([
      // "Tag ID": "2",
      // "Tag Name": "System Architect",
      // "Tag Type": "Edinburgh",
      // "My Feed": "32",
      // "My Favourites": "2011/04/25",
      // "Actions": "$3,120",
      "2",
      "Mile",
      "Edingburg",
      "32",
      "dddd",
      "5125125"
    ]).draw();

  })
});
