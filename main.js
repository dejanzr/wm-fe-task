$(document).ready(function() {

  /*  Add checked class used as check mark icon.
  *   @params
  *   tdNumber {Number} - number representing td.
   */
  function addCheckedClass(tdNumber) {
    var Nodes = document.querySelectorAll("#root tbody tr td:nth-of-type(" + tdNumber + ")");
    Nodes.forEach(function(node) {
      if (node.textContent === "Yes") {
        node.className = "checked";
      }
      node.className += " styling-class";
    });
  }

  // Add classes to all tag names which will on click trigger modal with current content.
  function addTriggerClasses() {
    var Nodes = document.querySelectorAll("#root tbody tr td:nth-of-type(2)");
    Nodes.forEach(function(node) {
      node.classList.add("triger-edit");
    });
  }

  addTriggerClasses();

  // When new entry is created check for td textContent and add classes.
  function addClassesToNewEntry() {
    var lastAddedEl = document.querySelectorAll("#root .data-tbody tr:last-of-type")[0].children,
      lastAddedFeed = lastAddedEl[3],
      lastAddedFavorites = lastAddedEl[4],
      lastAddedTagName = lastAddedEl[1];
    if (lastAddedFeed.textContent === "Yes") {
      lastAddedFeed.classList.add("checked", "styling-class");
    }
    if (lastAddedFavorites.textContent === "Yes") {
      lastAddedFavorites.classList.add("checked", "styling-class");
    }
    lastAddedTagName.classList.add("triger-edit");
  }


  addCheckedClass(5);
  addCheckedClass(4);


  $("#root thead tr:eq(1) th:not(:eq(3),:eq(4),:eq(5))").each(function(i) {
    var title = $(this).text();
    $(this).html("<input type=\"text\" class=\Search" + title + "\" />");
    $("input", this).on("keyup change", function() {
      if (table.column(i).search() !== this.value) {
        table
          .column(i)
          .search(this.value)
          .draw();
      }
    });
  });

  var table = $("#root").DataTable({
    orderCellsTop: false,
    "bInfo": false, // Showing 1 of n entries - hide.
    "bSortCellsTop": false,
    dom: "frtlip", // Show entries.
    orderCellsTop: true,
    "autoWidth": false,
    "pagingType": "full_numbers", // Set pager first and last page button.
    "order": [
      [0, "asc"]
    ],
    "columnDefs": [
      { "width": "64px", "targets": [0,5] },
      { "width": "113px", "targets": [3,4]},
      {
        "data": null,
        "defaultContent": "<span class=\"editor_edit\" data-toggle=\"modal\" data-target=\"#modal\">Edit</span> <span class=\"editor_remove\">Delete</span>   ",
        "targets": -1
      }
    ],

    fixedColumns: true,
    language: {
      paginate: {
        first: "<span class=\"data-pagination-ico data-pagination-ico-first\">First</span>",
        previous: "<span class=\"data-pagination-ico data-pagination-ico-prev\">Previous</span>",
        next: "<span class=\"data-pagination-ico data-pagination-ico-next\">Next</span>",
        last: "<span class=\"data-pagination-ico data-pagination-ico-last\">Last</span>"
      }
    },

    initComplete: function() {
      this.api().columns([3, 4]).every(function() {
        var column = this;
        var select = $("<select><option value=\"\">All</option>" +
          "<option value=\"Yes\">Yes</option>" +
          "<option value=\"No\">No</option></select>")
          .appendTo($(column.header()))
          .on("change", function() {
            var val = $.fn.dataTable.util.escapeRegex(
              $(this).val()
            );
            column
              .search(val ? "^" + val + "$" : "", true, false)
              .draw();
          });

      });
    }
  });

  // Detached and appended select filters.
  $("#root thead tr:eq(1) th:eq(3)").empty();
  $("#root thead tr:eq(1) th:eq(4)").empty();
  $("#root thead tr:eq(0) .sorting:nth-of-type(4) select").detach().appendTo($("#root thead tr:eq(1) th:eq(3)"));
  $("#root thead tr:eq(0) .sorting:nth-of-type(5) select").detach().appendTo($("#root thead tr:eq(1) th:eq(4)"));


  $("#root .data-filters").detach().insertBefore($("#root thead tr"));

  // Remove clicked row.
  $("#root").on("click", ".editor_remove", function() {
    table.row($(this).parents("tr")).remove().draw(false);
  });

  var editingRow;
  $("#root").on("click", ".editor_edit", function() {
    editingRow = this.parentNode.parentNode.children;
    var selectedRow = this.parentNode.parentNode.children,
      id = selectedRow[0].textContent,
      tagName = selectedRow[1].textContent,
      tagType = selectedRow[2].textContent,
      myFeed = selectedRow[3],
      myFavorites = selectedRow[4],
      tagNameValue = document.getElementById("name"),
      tagTypeValueSelected = document.getElementById("tag-select");
    myFeedCheckbox = document.querySelector("#myfeed"),
      myFavoritesCheckbox = document.querySelector("#myfavorites"),

      tagNameValue.value = tagName;
    tagTypeValueSelected.value = tagType;
    if (myFeed.className.includes("checked")) {
      myFeedCheckbox.checked = true;
    }
    if (myFavorites.className.includes("checked")) {
      myFavoritesCheckbox.checked = true;
    }


  });

  // Generate random ID for row.
  function genereateID() {
    return Math.floor(Math.random() * 1000) + 1;
  }

  // After new entry is saved clear all values.
  function clearTagValues() {
    var tagNameValue = document.querySelector("#name").value = "",
      myFeed = document.querySelector("#myfeed").checked = false,
      myFavorites = document.querySelector("#myfavorites").checked = false;
  }

  var btnSave = document.querySelector(".btn-modal-save");


  btnSave.addEventListener("click", function() {


    var tagNameValue = document.querySelector("#name").value,
      tagTypeValueSelected = document.querySelector("#tag-select").value,
      myFeed = document.querySelector("#myfeed").checked,
      myFavorites = document.querySelector("#myfavorites").checked,
      modal = document.querySelector("#modal");

    // Validation for tag name.
    if (tagNameValue === "") {
      alert("Tag name field is required.");
      return;
    }

    // Editing entry.
    if (editingRow !== undefined) {
      editingRow[1].textContent = tagNameValue;
      editingRow[2].textContent = tagTypeValueSelected;
      if (myFeed === false) {
        editingRow[3].classList.remove("checked");
        editingRow[3].innerText = "";
      }
      if (myFavorites === false) {
        editingRow[4].classList.remove("checked");
        editingRow[4].innerText = "";
      }
      if (myFeed === true) {
        editingRow[3].classList.add("checked");
      }
      if (myFavorites === true) {
        editingRow[4].classList.add("checked");
      }
    }
    else {
      // Adding new entry.
      var newEntry = table.row.add({
        0: genereateID(),
        1: tagNameValue,
        2: tagTypeValueSelected,
        3: myFeed ? "Yes" : null,
        4: myFavorites ? "Yes" : null,
        5: "5125125"
      }).draw();
      // Go to last page.
      table.order([0, "asc"]).draw();
      table.page("last").draw(false);
      addClassesToNewEntry();
    }

    // Close modal.
    $("#modal").modal("toggle");
    clearTagValues();
  });

  // Trigger edit from tag name.
  var tagNameTrigger = document.querySelector(".triger-edit");
  document.addEventListener("click", function(event) {
    if (event.target.classList.contains("triger-edit")) {
      event.target.parentElement.lastElementChild.firstElementChild.click();
    }
  });
});
