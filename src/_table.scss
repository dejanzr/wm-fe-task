table#root {

  &.no-footer {
    border: 1px solid $alto;
  }

  thead {
    background-color: $white;

    th, td {
      color: $dove-gray;
      font-size: 12px;
      border-bottom-color: $alto;
    }
  }

  th, td {
    &:not(:last-child) {
      border-right: 1px solid $alto;
    }
  }

  .data-filters th {
    padding: 0;
  }
}

.dataTables_length {
  margin-top: 15px;

  label {
    font-size: 12px;
    font-weight: 400;
  }

  select {
    width: 65px;
    height: 28px;
    font-size: 14px;
    border: 1px solid $silver;
    background-color: $white;
    border-radius: 0;
    box-shadow: none;
    transition: box-shadow .5s ease-in-out;
    cursor: pointer;
    padding: {
      right: 3px;
      left: 3px;
    }
    margin: {
      right: 3px;
      left: 3px;
    }

    &:focus {
      outline: none;
      box-shadow: inset 0 0 0 2px $cornflower-blue;
    }
  }
}

.SearchTag {
  width: 100%;
  height: 34px;
  border: none;
  border-radius: 0;
  background-image: url("../icons/search.svg");
  background-size: 14px 14px;
  background-repeat: no-repeat;
  background-position: center right 10px;
  appearance: none;
  box-shadow: none;
  transition: box-shadow .5s ease-in-out;
  padding: {
    top: 10px;
    right: 10px;
    bottom: 10px;
    left: 10px;
  }

  &:focus {
    outline: none;
    box-shadow: inset 0 0 0 2px $cornflower-blue;
  }
}

.dataTables_wrapper {

  thead th {
    background-color: $white;

    &:not(:nth-child(2)):not(:nth-child(3)) {
      text-align: center;
    }
  }

  .dataTable {
    border: 1px solid $alto;
  }

  .tr-actions {
    background-color: $carara;
    text-indent: -9999px;
  }
}

.data-filters {

  th {
    padding: 0;
  }

  select {
    width: 100%;
    border: none;
    border-radius: 0;
    cursor: pointer;
    text-align: center;
    text-align-last: center;
    background-color: $white;
    box-shadow: none;
    transition: box-shadow .5s ease-in-out;
    padding: {
      top: 9px;
      right: 10px;
      bottom: 9px;
      left: 10px;
    }

    &:focus {
      outline: none;
      box-shadow: inset 0 0 0 2px $cornflower-blue;
    }
  }
}

.dataTable {

  tr[role=row] {

    &.data-filters {
      th:nth-child(6) {
        text-indent: -9999px;
        background-color: $carara;
      }
    }

    &:not(.data-filters) {

      th {
        padding: {
          top: 9px;
          bottom: 8px;
        }

        &:nth-child(2),
        &:nth-child(3) {
          padding: {
            right: 10px;
            left: 10px;
          }
        }
      }

      td {
        padding: {
          top: 6px;
          bottom: 5px;
        }

        &:nth-child(1) {
          font-size: 13px;
        }

        &:nth-child(1),
        &:nth-child(6) {
          width: 6%;
          text-align: center;
        }

        &:nth-child(2),
        &:nth-child(3) {
          width: 34%;
          font-size: 13px;
        }

        &:nth-child(4),
        &:nth-child(5) {
          width: 9%;
        }
      }
    }
  }
}
